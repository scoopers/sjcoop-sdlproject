/**
 * Author: Sam Cooper
 */
/**
 * Main Function, Testing scripts are run here
 */

#include <stdio.h>
#include "../inc/treeNode.h"
#include "../inc/list.h"
#include "../inc/qtreeOperations.h"
#include "../inc/print.h"
#include "../inc/destroy.h"
#include "../inc/valueTree.h"

int main(int argc, char *argv[]) {
  int i;
  Quadtree *tree = initTree(); //making an initial tree

  listGrowTree(tree);

  //generateModel(tree, 0.5, 0); //model choice 0, tolerance 0.5
  generateModel(tree, 0.2, 1); //model choice 1, tolerance 0.2

  //new list is created after changes made
  createList(tree);
  listWriteTree(tree);
  destroyTree(tree);
  return 0;
}
