#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../inc/qtreeOperations.h"
#include "../inc/list.h"
#include "../inc/valueTree.h"

const int MAX_LEVEL = 7;

/**
 * Tree manipulation functions
 */

Node *makeNode(double x, double y, int level) {
  int i;
  // if max level is reached, return NULL
  if(level > MAX_LEVEL){
    Node *node = NULL;
    return node;
  }
  // allocate the data structure
  Node *node = (Node *)malloc(sizeof(Node));
  // set the node data
  node->level = level;
  node->xy[0] = x;
  node->xy[1] = y;
  // set children to NULL
  for(i=0; i<4; ++i){
    node->child[i] = NULL;
  }
  return node;
}

// creates four children for a given node
void makeChildren(Node *parent) {
  // parent data
  double x = parent->xy[0];
  double y = parent->xy[1];
  int level = parent->level;
  // child edge length
  double hChild = pow(2.0,-(level+1));
  // create children at level+1
  parent->child[0] = makeNode( x,y, level+1);
  parent->child[1] = makeNode( x+hChild,y, level+1);
  parent->child[2] = makeNode( x+hChild,y+hChild, level+1);
  parent->child[3] = makeNode( x,y+hChild, level+1);
  return;
}

// cycles through tree, uniformly adding children to each leaf node
void growTree(Node *node){
  int i;
  if(node->child[0] == NULL) {
    makeChildren(node);
  } else {
    for(i=0; i<4; ++i) {
      growTree(node->child[i]);
    }
  }
  return;
}

// list version of growTree() function
void listGrowTree(Quadtree *tree) {
  createList(tree);
  Node *walk = tree->list->topLeaf;
  // traverses list, uniformly growing leaf node, as long as leaf node isn't NULL
  for( ; walk != NULL; walk = walk->nextLeaf) {
      makeChildren(walk);
  }
  return;
}

// initialises the tree with head of level 0 and NULL list
Quadtree *initTree() {
  // allocates memory for the tree struct
  Quadtree *tree = (Quadtree *)malloc(sizeof(Quadtree));
  tree->head = makeNode(0.0, 0.0, 0);
  tree->head->nextLeaf = NULL;
  tree->list = initList();
  return tree;
}

// traverses linked list of nodes, adding children if indicator() returns false
int passThrough(Node* topLeaf, double tolerance, int choice) {
  int numFalse = 0;
  // traverses list, if indication() is false for any given node, children are added
  while(topLeaf != NULL) {
      bool indication = indicator(topLeaf, tolerance, choice);
      if(indication == false) {
        makeChildren(topLeaf);
        // number of false returns inremented
        ++numFalse;
      }
      topLeaf = topLeaf->nextLeaf;
  }
  return numFalse;
}

// using passThrough(), generates a data dependent tree based on choices made by the user
void generateModel(Quadtree *tree, double tolerance, int choice) {
  int numFalse;
  // loop continuously calls passThrough() until numFalse is 0
  do {
    createList(tree);
    numFalse = passThrough(tree->list->topLeaf, tolerance, choice);
  } while(numFalse > 0);
  return;
}
