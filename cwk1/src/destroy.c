#include <stdio.h>
#include <stdlib.h>
#include "../inc/destroy.h"

/**
 * Tree destruction
 */

//free memory of single node
void destroyNode(Node *node) {
  free(node);
  return;
}

//recursively frees memory of tree underneath the specified node
void destroyProcedure(Node *node) {
  int i;
  if(node->child[0] != NULL) {
    for(i=0; i<4; ++i) {
      destroyProcedure(node->child[i]);
      destroyNode(node->child[i]);
    }
  }
  return;
}

//frees memory of entire tree
void destroyTree(Quadtree *tree) {
  if(tree->head == NULL) {
    printf("Cannot destroy NULL tree\n");
    return;
  }
  destroyProcedure(tree->head);
  free(tree->head);
  free(tree->list);
  free(tree);
  return;
}
