/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#ifndef SDLSTRUCTS_H
#define SDLSTRUCTS_H

/* WINDOW DIMENSIONS */
#define SCREEN_WIDTH 1080
#define SCREEN_HEIGHT 720

/* NUMBER OF BUTTONS*/
#define NUM_BUTTONS 15

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#ifdef CENTRE_FILE

/* If in file globals will live in, define them */
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Texture* paletteSprites = NULL;
SDL_Surface* tPaletteSprites = NULL;
TTF_Font* font = NULL;
int brushSize = 1;

#else

/* SDL STRUCTURES */
// window of program
extern SDL_Window* window;
// renderer to render pixels
extern SDL_Renderer* renderer;
// surface for temp sprite sheet palette
extern SDL_Surface* tPaletteSprites;
// texture to better hold spriteSheet
extern SDL_Texture* paletteSprites;
// font used
extern TTF_Font* font;

#endif

/* VARIABLE BRUSH SIZE */
extern int brushSize;

#endif
