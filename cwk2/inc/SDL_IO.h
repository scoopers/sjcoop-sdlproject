/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#ifndef SDLIO_H
#define SDLIO_H

// returns current time + .bmp as a string
void timeFileName();
// saves current worksheet to bmp file
void save();

#endif
