/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdbool.h>
#include <stdio.h>

#include "../inc/SDL_Structures.h"
#include "../inc/SDL_Ops.h"

/* initialise SDL structures (window, renderer, canvas) returns false if failure */
bool init() {
  // condition for error handling
  bool succeed = true;

  // error checking, assuring reasons are given
  if(SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL could not initialise! SDL_Error: %s\n", SDL_GetError());
    succeed = false;
  } else {
    // create window and renderer
    window = SDL_CreateWindow("Grapix", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT + 80, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE || SDL_RENDERER_ACCELERATED);
    // set colour for background and apply to renderer
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    // setting window icon
    SDL_Surface* icon = IMG_Load("assets/icon.png");
    SDL_SetWindowIcon(window, icon);
    SDL_FreeSurface(icon);

    // load font
    if(TTF_Init() == -1) {
      printf("Could not initialise TTF!\n");
      succeed = false;
    } else {
      font = TTF_OpenFont("assets/font.ttf", 32);
    }

    // if by chance window is assigned NULL, case is handled
    if(window == NULL) {
      printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
      succeed = false;
    }
  }
  return succeed;
}


/* returns screen height (for modules without access) */
int getScreenHeight() {
  return SCREEN_HEIGHT;
}


/* returns true if left mouse button is held down */
bool leftMouseDown() {
  return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT);
}


/* destroys SDL structures and resets memory */
void closeSDL() {
  // deallocate renderer memory and clean up
  SDL_DestroyRenderer(renderer);
  renderer = NULL;

  // destroy window
  SDL_DestroyWindow(window);
  window = NULL;

  // destroy spitesheet texture
  SDL_DestroyTexture(paletteSprites);

  // destroy font
  TTF_CloseFont(font);
  font = NULL;

  // quit SDL
  TTF_Quit();
  SDL_Quit();
  return;
}
