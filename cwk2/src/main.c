/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../inc/SDL_Ops.h"
#include "../inc/SDL_Render.h"
#include "../inc/SDL_UI.h"

int main(int argc, char const *argv[]) {

  // loop condition variable for closing program
  bool quit = false;
  // representing an event (quit, mouse event)
  SDL_Event event;
  // position of mouse
  int gLastXY[2] = {0, 0};

  // initialise SDL
  if(!init()) {
    return -1;
  } else {
    // palette rendering
    initPalette();
    displayPalette();
    // for testing
    initBrushColour();
    while(!quit) {
      // update mouse state (so lines don't connect between button presses)
      SDL_GetMouseState(&gLastXY[0], &gLastXY[1]);
      // event handling
      SDL_WaitEvent(&event);
      switch (event.type) {
        case SDL_QUIT: quit = true;
        break;
        case SDL_MOUSEBUTTONDOWN:
        if(leftMouseDown() && (event.button.y > getScreenHeight())) {
          buttonChange(isOverButton(event.button.x, event.button.y));
        }
        case SDL_MOUSEMOTION:
          if(leftMouseDown() && (event.button.y < getScreenHeight())) {
            draw(&gLastXY[0], &gLastXY[1]);
          }
        break;
      }
      render();
    }
  }
  closeSDL();
  return 0;
}
