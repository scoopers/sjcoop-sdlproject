#include <SDL2/SDL.h>
#include <stdio.h>

#include "data.h"
#include "SDLopen.h"
// screen dimensions
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init() {
  // condition for error handling
  bool succeed = true;

  if(SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL could not initialise! SDL_Error: %s\n", SDL_GetError());
    succeed = false;
  } else {
    // create window
    window = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

    if(window == NULL) {
      printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
      succeed = false;
    } else {
      // get surface
      screenSurface = SDL_GetWindowSurface(window);
    }
  }
  return succeed;
}

// temporary for learning SDL
bool loadImage(char *filepath) {
  bool succeed = true;
  // load image
  image = SDL_LoadBMP(filepath);
  if(image == NULL) {
    printf("Unable to load image %s! SDL Error: %s\n", filepath, SDL_GetError());
    bool succeed = false;
  }
  return succeed;
}

void displayImage() {
  // put image on surface
  SDL_BlitSurface(image, NULL, screenSurface, NULL);

  // update the surface
  SDL_UpdateWindowSurface(window);
}

void closeSDL() {
  // deallocate surface memory and clean up
  SDL_FreeSurface(image);
  image = NULL;

  // destroy window
  SDL_DestroyWindow(window);
  window = NULL;

  // quit SDL
  SDL_Quit();
}
