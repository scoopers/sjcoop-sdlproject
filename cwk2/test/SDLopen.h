#ifndef SDLOPEN_H
#define SDLOPEN_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdbool.h>


bool init();
bool loadImage();
void displayImage();
void closeSDL();

#endif
