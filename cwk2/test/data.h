#ifndef DATA_H
#define DATA_H

/* SDL Structures */
// main window
SDL_Window* window = NULL;
//surface in window
SDL_Surface* screenSurface = NULL;
//window we load and show
SDL_Surface* image = NULL;
// renderer for media
SDL_Renderer* renderer = NULL;
// texture for media
SDL_Texture* canvas = NULL;

#endif
